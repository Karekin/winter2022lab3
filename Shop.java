import java.util.Scanner;
public class Shop{
	
	public static void main(String[] args){
		Scanner s = new Scanner(System.in);
		
		Sandwhich[] sandwhiches = new Sandwhich[4];
		for(int i = 0; i < 4; i++){
			
			sandwhiches[i] = new Sandwhich();
			
			System.out.println("What is the type of meat you will have?");
			sandwhiches[i].meat = s.next();
			
			System.out.println("What is the size of your sandwhich in inches?");
			sandwhiches[i].size = Integer.parseInt(s.next());
			
			System.out.println("What cheese would you like?");
			sandwhiches[i].cheese = s.next();
		}
		
		System.out.println(sandwhiches[3].meat);
		System.out.println(sandwhiches[3].size);
		System.out.println(sandwhiches[3].cheese);
		
		sandwhiches[3].eat();
	}
}