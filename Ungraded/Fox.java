public class Application{
	
	public static void main(String[] args){
		
		Fox redFox = new Fox();
		Fox arcticFox = new Fox();
		
		redFox.population = 3646;
		redFox.diet = "Omnivore";
		redFox.marine = true;
		
		arcticFox.population = 965;
		arcticFox.diet = "Carnivore";
		arcticFox.marine = false;
		
		System.out.println(redFox.population);
		System.out.println(redFox.diet);
		System.out.println(redFox.marine);
		System.out.println(arcticFox.population);
		System.out.println(arcticFox.diet);
		System.out.println(arcticFox.marine);
		
		redFox.eat();
		arcticFox.eat();
		redFox.run();
		arcticFox.run();
		
		Fox[] skulk = new Fox[3];
		
		skulk[0] = redFox;
		skulk[1] = arcticFox;
		
		skulk[2] = new Fox();
		skulk[2].population = 2478;
		skulk[2].diet = "Omnivore";
		skulk[2].marine = false;
		System.out.println(skulk[2].population);
		System.out.println(skulk[2].diet);
		System.out.println(skulk[2].marine);
	}
}